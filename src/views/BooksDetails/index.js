import BooksDetails from "./BooksDetails";
import { connect } from 'react-redux';
import axios from "axios"

const mapStateToProps = (dispatch) => ({
  
 async getBookById (id) {
    const path = "http://api.tvmaze.com/shows/";
    try {
      const  {data}  = await axios.get(`${path}${id}`);
       const infoBook = {
          id: data.id,
          image: data.image ? data.image.original : null,
          title: data.name,
          genres: data.genres.join(", "),
          description: data.summary
       }
       console.log(infoBook);
      return infoBook
    } catch (error) {
      console.log(error);
    }
  }
  
})

export default connect(mapStateToProps)(BooksDetails);
