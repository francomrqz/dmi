
import React, { useEffect, useState } from 'react';

const BooksDetails = ({ getBookById }) => {

  const [bookData, addBook] = useState({
    image: null,
    description: "",
    genres: "",
    title: ""
  })

  const getData = async () => {
    const strs = window.location.href.split('/');
    const id = strs.at(-1)
    if (id) {
      const response = await getBookById(id);
      addBook(response)
    }
  }

  useEffect(() => {
    getData()
    // eslint-disable-next-line
  }, []);

  return (
    <div className="container px-15">
      <div className="row">
        {bookData.image ?
          <div>
            <img alt={bookData.title} src={bookData.image} width="350px" />
          </div>
          : ""}
        <div className="col-6 ">
          <div className="pl-15">
            <h2>{bookData.title} </h2>
            <div dangerouslySetInnerHTML={{ __html: bookData.description }} />
            <p>
              <h5>{bookData.genres}</h5>
            </p>
            <a href="/" className="link"> Back </a>
          </div>
        </div>

      </div>
    </div>
  )
};
export default BooksDetails;