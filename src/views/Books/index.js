import Books from "./Books";
import { connect } from 'react-redux';
import axios from "axios"

const mapStateToProps = (dispatch) => ({

  async getBooks() {
    const path = "http://api.tvmaze.com/search/shows?q=girls";
    try {
      const { data } = await axios.get(path);

      const mapBooks = data.map((item) => {
        return {
          id: item.show.id,
          thumbnail: item.show.image ? item.show.image.medium : "",
          title: item.show.name,
          description: item.show.summary
        }
      });
      return mapBooks
    } catch (error) {
      console.log(error);
    }
  },

})

export default connect(mapStateToProps)(Books);
