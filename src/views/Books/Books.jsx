import React, { useEffect, useState } from 'react';
import styles from "./Books.module.scss";

const Books = ({ getBooks }) => {

  const [booksList, addBooks] = useState([])

  const getData = async () => {
    const listOfBooks = await getBooks();
    addBooks(listOfBooks)
  }

  useEffect(() => {
    getData()
    // eslint-disable-next-line 
  }, []);

  return (
    <div className="container ">
      <div className={`row ${styles.flex}`}>
        {
          booksList.map((item, index) => (
            <div key={index} className={`flex-column  col-6`}>
              <div className="px-15 pb-10 mb-10 min-height-col">
                <h3>{item.title}</h3>
                <div className="d-flex">
                  <div className="pr-15">
                    <img alt={item.title} src={item.thumbnail} width="150px" />
                  </div>
                  <div className="align-self-start">
                    <div dangerouslySetInnerHTML={{ __html: item.description }} />
                    <a href={`/${item.id}`} className="link btn pr-15 float-right">Read more</a>
                  </div>
                </div>
              </div>
            </div>
          ))}

      </div>
    </div>

  )
};
export default Books;