import { BrowserRouter, Route, Routes } from "react-router-dom";
import Books from "./../../views/Books";
import BooksDetails from "./../../views/BooksDetails";

import Header from "../Header";
const Frame = (props) => {
  return (
    <BrowserRouter>
      <div className="container">
        <Header />
        <Routes>
          <Route path="/" element={<Books />}>
            {props.children}
          </Route>
          <Route path="/:id" element={<BooksDetails />}>
            {props.children}
          </Route>
        </Routes> 
      </div>
    </BrowserRouter>
  )
};
export default Frame;