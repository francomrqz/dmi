import { shallow } from 'enzyme';
import Header from '../Header';

it("Is renders without crashing", () => {
  shallow(<Header />);
});

it("is renders Books link on header", () => {
  const wrapper = shallow(<Header />);
  const header = <a href="/books" >Books</a>;
  expect(wrapper.contains(header)).toEqual(true);
});

test('should show title', () => {
  const wrapper = shallow(<Header />);
  const text = wrapper.find('a');
  expect(text.text()).toBe("Books");
});