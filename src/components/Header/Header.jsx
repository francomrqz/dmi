import styles from "./Header.module.scss"

const Header = () => {
  return (
    <ul className={` ${styles.Header}`}>
      <li>
        <a href="/books" >
          Books
        </a>
      </li>
    </ul>
  )

}

export default Header;