import { createReducer } from "@reduxjs/toolkit";
import { getBooks, deleteBook } from "../actions/booksActions"
const initialState = {
  books: []
};

const bookReducer = createReducer(initialState, (builder) => {
  builder
    .addCase(getBooks, (state, action) => {
      state.books.push(action.books);
    })
    .addCase(deleteBook, (state, action) => {
      state.books = state.books.filter(book => book.id !== action.book.id)
    })

});

export default bookReducer;

