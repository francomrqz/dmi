import { createAction } from "@reduxjs/toolkit";


export const getBooks = createAction("getBooks");
export const deleteBook = createAction("deleteBook");
