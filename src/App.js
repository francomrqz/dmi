import Frame from './components/Frame';
import { Provider } from 'react-redux';

import store from './store';
import './App.css';
function App() {
  return (
    <div className="App">
      <Provider store={store}>
        <Frame />
      </Provider>
    </div>
  );
}

export default App;
